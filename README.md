# VersaComms API Library

[![NuGet Version](https://img.shields.io/nuget/v/VersaComms.svg)](https://www.nuget.org/packages/VersaComms)
[![License](https://img.shields.io/github/license/yourusername/VersaComms)](https://github.com/yourusername/VersaComms/blob/main/LICENSE)

<img src="images/versa_comms_logo.png" width="400" height="300">

VersaComms (Versatile Communication) is a versatile and comprehensive API library for multi-channel message sending. It enables developers to easily send messages via various channels, including SMS, phone calls, texts, and social media platforms. Written in C#, this library streamlines message sending operations within your .NET applications.


## Features

- **Multi-Channel Support**: Send messages seamlessly across multiple channels, including SMS, phone calls, texts, and popular social media platforms.
- **Unified API**: Utilize a consistent and intuitive API for sending messages across diverse channels, simplifying development and enhancing productivity.
- **Extensibility**: Extend the library effortlessly to support additional messaging channels or custom integrations.
- **Flexible Configuration**: Configure various settings, such as API keys, authentication, to meet your specific requirements.

## Installation

The VersaComms library is available as a NuGet package. You can install it using the NuGet Package Manager or by running the following command in the Package Manager Console:

```bash
Install-Package VersaComms
```

## Build

Build the class library with command:
```
dotnet build lib
```

### Targeted platforms

- netcoreapp2.2
- net4.6
- net6.0

## Usage

To get started with VersaComms, refer to the documentation for guidelines and tests for comprehensive examples. The documentation provides detailed instructions on library usage, message sending configuration, and integration with different messaging channels.

Below is a simple example demonstrating how to send an SMS message using VersaComms:

```csharp
using VersaComms;

// Create a VersaComms client
VersaClient client = new VersaClient("YourApiKey");

// Prepare the message data
Message message = new Message(content: "Hello World!");

// Prepare communication participants
Communicator sender = new Communicator(id: 1, contact: new ContactInfo(phoneContact: "+123456789"));
Communicator reciever = new Communicator(id: 2, contact: new ContactInfo(phoneContact: "+123459999"));


// Send the message
MessageStatus result = client.sendMessage(message, ChannelType.SMS, sender, reciever);

// Check the response status
if (result == MessageStatus.Sent)
{
    Console.WriteLine("Message sent successfully!");
}
else
{
    Console.WriteLine("Message sending failed.");
}
```

## Testing

Run unit tests by using terminal command:
```
dotnet test tests
```

## Contributing

Contributions are welcome! If you encounter any issues, have suggestions, or would like to contribute enhancements or new features, please create an issue or submit a pull request.

Before contributing, please review the contribution guidelines.
## License

This library is released under the MIT License.