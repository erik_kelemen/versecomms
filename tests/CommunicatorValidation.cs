using NUnit.Framework;
using VersaComms;

namespace VersaComms.tests;

[TestFixture]
public class CommunicantValidationTests
{
    private VersaClient VersaAPI;

    [SetUp]
    public void Setup()
    {
        VersaAPI = new VersaClient("test");
    }

    [Test]
    public void MissingContact()
    {
        // Arrange Communicants
        Communicator sender = new Communicator(id: 1, contact: new ContactInfo(phoneContact: "+123456789"));
        Communicator reciever = new Communicator(id: 1, contact: new ContactInfo());
        Message message = new Message(content: "Hello World!");

        try {
            // Act
            MessageStatus result = VersaAPI.sendMessage(message, ChannelType.SMS, sender, reciever);
        } catch (Exception e) {
            // Assert
            Assert.AreEqual("Reciever does not have a phone contact.", e.Message);
        }
    } 

    [Test]
    public void MissingAccess()
    {
        // Arrange Communicants
        Communicator sender = new Communicator(id: 1, contact: new ContactInfo(phoneContact: "+123456789"));
        Communicator reciever = new Communicator(id: 2, contact: new ContactInfo(phoneContact: "+123459999"));
        Message message = new Message(content: "Hello World!");

        try {
            // Act
            MessageStatus result = VersaAPI.sendMessage(message, ChannelType.SMS, sender, reciever);
        } catch (Exception e) {
            // Assert
            Assert.AreEqual("Access type TwilioSMS not granted.", e.Message);
        }
    } 

    [Test]
    public void MissingChannel()
    {
        // Arrange Communicants
        Communicator sender = new Communicator(id: 1, contact: new ContactInfo(emailContact: "abc@domain.info"));
        Communicator reciever = new Communicator(id: 2, contact: new ContactInfo(emailContact: "rnb@domain.cz"));
        Message message = new Message(content: "Hello World!");

        try {
            // Act
            MessageStatus result = VersaAPI.sendMessage(message, ChannelType.Email, sender, reciever);
        } catch (Exception e) {
            // Assert
            Assert.AreEqual("The method or operation is not implemented.", e.Message);
        }
    } 

    [Test]
    public void AdapterAuth()
    {
        // Arrange Communicants
        Communicator sender = new Communicator(id: 1, contact: new ContactInfo(phoneContact: "+123456789"));
        Communicator reciever = new Communicator(id: 2, contact: new ContactInfo(phoneContact: "+123459999"));
        Message message = new Message(content: "Hello World!");
        sender.Contact.PhoneContact.addAccess(ContactAccessType.TwilioSMS, new TwilioSMSAccess("test-sid", "test-token"));

        try {
            // Act
            MessageStatus result = VersaAPI.sendMessage(message, ChannelType.SMS, sender, reciever);
        } catch (Exception e) {
            // Assert
            Assert.AreEqual("Authentication Error - invalid username", e.Message);
        }
    } 
}
