using VersaComms;
using VersaComms.Ports;
using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
namespace VersaComms.Adapters {

    /// <summary>
    /// TwilioSMSAdapter is an adapter for Twilio SMS channel.
    /// </summary>
    public class TwilioSMSAdapter : IPortSMSChannel {
        /// <summary>
        /// sendMessage sends a message to target via Twilio SMS API.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="from">The communicator sending the SMS message.</param>
        /// <param name="to">The communicator receiving the SMS message.</param>
        public MessageStatus sendMessage(Message message, Communicator from, Communicator to) {
            validate(from, to, message);
            TwilioSMSAccess twilioAccess = (TwilioSMSAccess)from.getAccess(ContactAccessType.TwilioSMS);
            TwilioClient.Init(twilioAccess.AccountSID, twilioAccess.AuthToken);
            var messageResource = MessageResource.Create(
                body: message.Content.Content,
                from: new Twilio.Types.PhoneNumber(from.Contact.PhoneContact?.PhoneNumber),
                to: new Twilio.Types.PhoneNumber(to.Contact.PhoneContact?.PhoneNumber)
            );
            Console.WriteLine($"Sent SMS message from {from.Contact.PhoneContact?.PhoneNumber} to {to.Contact.PhoneContact?.PhoneNumber}.");

            //TODO: Process messageResource to Twilio API
            return MessageStatus.Sent;
        }

        public void validate(Communicator from, Communicator to, Message message) {
            if (from.Contact.PhoneContact == null) {
                throw new Exception("Sender does not have a phone contact.");
            }
            if (to.Contact.PhoneContact == null) {
                throw new Exception("Reciever does not have a phone contact.");
            }
            if (message.Content.Content == null) {
                throw new Exception("Message does not have content.");
            }
        }
    }
}
