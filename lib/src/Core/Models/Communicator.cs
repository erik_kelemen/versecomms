using System;
using VersaComms;
namespace VersaComms {

    /// <summary>
    /// Communicator is participant in a communication.
    /// </summary>
    /// <param name="id">The unique identifier of the communicator.</param>
    /// <param name="contact">The contact information of the communicator.</param>
    public class Communicator : IAudience {
        private int _id;
        public ContactInfo Contact { get; set; }
        public Communicator(int id, ContactInfo contact) {
            _id = id;
            Contact = contact;
        }

        public Communicator[] listCommunicators() {
            return new Communicator[] { this };
        }

        public IContactAccess getAccess(ContactAccessType accessType) {
            Contact? contact = null;
            switch (accessType) {
                case ContactAccessType.TwilioSMS:
                    contact = Contact.PhoneContact;
                    break;
                default:
                    throw new Exception($"Access type {accessType} not granted.");
            }
            if (contact == null) {
                throw new Exception($"Access type {accessType} not granted.");
            }
            return contact.getAccess(accessType);
        }
    }

    /// <summary>
    /// Audience is target for a communication.
    /// </summary>
    public interface IAudience {
        public Communicator[] listCommunicators();
    }

    public class ContactInfo {
        public EmailContactInfo? EmailContact { get; set; }
        public PhoneContactInfo? PhoneContact { get; set; }

        public ContactInfo(EmailContactInfo? emailContact = null, PhoneContactInfo? phoneContact = null) {
            EmailContact = emailContact;
            PhoneContact = phoneContact;
        }
    }

    public interface IContactAccess {

    }

    public class TwilioSMSAccess : IContactAccess {
        public string AccountSID { get; set; }
        public string AuthToken { get; set; }
        public TwilioSMSAccess(string accountSID, string authToken) {
            AccountSID = accountSID;
            AuthToken = authToken;
        }
    }
    public enum ContactAccessType {
        TwilioSMS,
    }

    public abstract class Contact {
        private Dictionary<ContactAccessType, IContactAccess> _accessCredentials;

        public Contact() {
            _accessCredentials = new Dictionary<ContactAccessType, IContactAccess>();
        }

        public IContactAccess getAccess(ContactAccessType accessType) {
            if (!_accessCredentials.ContainsKey(accessType)) {
                throw new Exception($"Access type {accessType} not granted.");
            }
            return _accessCredentials[accessType];
        }

        public void addAccess(ContactAccessType accessType, IContactAccess access) {
            _accessCredentials[accessType] = access;
        }
    }

    /// <summary>
    /// EmailContactInfo is contact information for an email address and its access credentials.
    /// </summary>
    /// <param name="email">The email address.</param>
    public class EmailContactInfo : Contact {
        public string EmailAddress { get; set; }
        private Dictionary<ContactAccessType, IContactAccess> _accessCredentials;
        public EmailContactInfo(string email) {
            EmailAddress = email;
            _accessCredentials = new Dictionary<ContactAccessType, IContactAccess>();
        }

        public static implicit operator EmailContactInfo(string email) {
            return new EmailContactInfo(email);
        }
    }

    /// <summary>
    /// PhoneContactInfo is contact information for a phone number and its access credentials.
    /// </summary>
    /// <param name="phoneNumber">The phone number.</param>
    public class PhoneContactInfo : Contact {
        public string PhoneNumber { get; set; }
        private Dictionary<ContactAccessType, IContactAccess> _accessCredentials;
        public PhoneContactInfo(string phoneNumber) {
            PhoneNumber = phoneNumber;
            _accessCredentials = new Dictionary<ContactAccessType, IContactAccess>();
        }

        public static implicit operator PhoneContactInfo(string phoneNumber) {
            return new PhoneContactInfo(phoneNumber);
        }
    }
}
