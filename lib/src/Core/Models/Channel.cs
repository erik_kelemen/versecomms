namespace VersaComms {

    /// <summary>
    /// A channel is a method of communication supported by VersaComms.
    /// </summary>
    public enum ChannelType {
        Email,
        SMS,
        Fax,
    }
}
