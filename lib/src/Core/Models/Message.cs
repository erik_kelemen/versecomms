namespace VersaComms {
    
    /// <summary>
    /// Message is communicated content.
    /// </summary>
    /// <param name="content">The content of the message.</param>
    /// <param name="type">The type of the message.</param>
    public class Message {
        public MessageContent Content { get; set; }
        public MessageType Type { get; set; }

        public Message(MessageContent content, MessageType type = MessageType.Unknown) {
            Content = content;
            Type = type;
        }
    }

    /// <summary>
    /// MessageStatus is the status of a message.
    /// </summary>
    public enum MessageStatus {
        Unknown,
        Sent,
        Delivered,
        Failed
    }

    /// <summary>
    /// MessageType is the type of a message supported by VersaComms.
    /// </summary>
    public enum MessageType {
        Unknown
    }

    /// <summary>
    /// IMessageContent is interface for content of a message.
    /// </summary>
    public abstract class MessageContent {
        public string? Content { get; set; }

        public static implicit operator MessageContent(string content) {
            return new TextMessageContent(content);
        }
    }

    /// <summary>
    /// TextMessageContent is content of a text message.
    /// </summary>
    public class TextMessageContent : MessageContent {
        public TextMessageContent(string content) {
            Content = content;
        }

        public static implicit operator TextMessageContent(string content) {
            return new TextMessageContent(content);
        }
    }

}

