using VersaComms;
using VersaComms.Ports;
namespace VersaComms {
    /// <summary>
    /// VersaClient is a communication service API.
    /// </summary>
    public class VersaClient {
        private string _apiKey;

        private PortSMSChannel _portSMSChannel;
        public VersaClient(string apiKey) {
            _apiKey = apiKey;
            _portSMSChannel = new PortSMSChannel();
        }

        /// <summary>
        /// sendMessage sends a message to port based on channel type.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="channel">The channel to send the message through.</param>
        /// <param name="from">The communicator sending the message.</param>
        /// <param name="to">The audience to send the message to.</param>
        public MessageStatus sendMessage(Message message, ChannelType channel, Communicator from, IAudience to) {
            MessageStatus status = MessageStatus.Unknown;
            switch (channel) {
                case ChannelType.Email:
                    throw new NotImplementedException();
                case ChannelType.SMS:
                    status = _portSMSChannel.sendMessage(message, from, to);
                    break;
                case ChannelType.Fax:
                    throw new NotImplementedException();
                default:
                    throw new NotImplementedException();
            }
            // TODO: Send status to analytics.
            return status;
        }
    }
}
