using VersaComms;
using VersaComms.Adapters;
namespace VersaComms.Ports;

/// <summary>
/// PortSMSChannel is a port for SMS channel.
/// </summary>
public class PortSMSChannel {
    private IPortSMSChannel? _portSMSChannelAdapter;

    /// <summary>
    /// sendMessage sends a message to an audience.
    /// </summary>
    /// <param name="message">The message to send.</param>
    /// <param name="from">The communicator sending the message.</param>
    /// <param name="to">The audience to send the message to.</param>
    public MessageStatus sendMessage(Message message, Communicator from, IAudience to) {
        if (_portSMSChannelAdapter == null) {
            throw new NotImplementedException();
        }
        MessageStatus[] statuses = new MessageStatus[to.listCommunicators().Length];
        for (int i = 0; i < to.listCommunicators().Length; i++) {
            Console.WriteLine($"Sending SMS message from {from.Contact.PhoneContact?.PhoneNumber} to {to.listCommunicators()[i].Contact.PhoneContact?.PhoneNumber}.");
            statuses[i] = _portSMSChannelAdapter.sendMessage(message, from, to.listCommunicators()[i]);
        }
        return statuses[0]; // TODO: Return the most severe status.
    }

    public PortSMSChannel() {
        _portSMSChannelAdapter = new TwilioSMSAdapter();
    }
}

/// <summary>
/// IPortSMSChannel is interface for SMS channel port adapter.
/// </summary>
public interface IPortSMSChannel {
    public MessageStatus sendMessage(Message message, Communicator from, Communicator to);
}